package com.passmann;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CompareLists 
{
	/*
	 * Checks if there are entries occurring ONLY in the first passed list/parameter
	 * 
	 * @param list1 List<String> object containing a text file
	 * @param list1 List<String> object containing a text file
	 * @return onlyInThisList List<String> object with occurrences in first list which has been passed
	 */
	
	public static List<String> checkUniqueOccurence(List<String> list1, List<String> list2)
	{
		List<String> onlyInThisList = new ArrayList<String>();
		
		for(int i = 0; i < list1.size(); i++)
		{
			if(!list2.contains(list1.get(i)))
			{
				onlyInThisList.add(list1.get(i));
			}
		}
		
		return onlyInThisList;
	}
	
	/*
	 * Checks if there are entries occurring in both lists
	 * 
	 * @param list1 List<String> object containing a text file
	 * @param list1 List<String> object containing a text file
	 * @return inBothLists List<String> object with occurrences in both lists
	 */
	
	public static List<String> checkMatchesInBothLists(List<String> list1, List<String> list2)
	{
		List<String> inBothLists = new ArrayList<String>();

		for(int i = 0; i < list1.size(); i++)
		{
			if(list2.contains(list1.get(i)))
			{
				inBothLists.add(list1.get(i));
			}
		}
		
		return inBothLists;
	}
	
	/*
	 * Removes all duplicate entries of the passed List<String> object
	 * 
	 * @param list List<String> object containing input of one text file
	 * @return noDuplicates List<String> object without duplicates
	 */
	
	public static List<String> removeDuplicates(List<String> list)
	{
		List<String> noDuplicates = new ArrayList<String>();
		
		for(int i = 0; i < list.size(); i++)
		{
			if(!noDuplicates.contains(list.get(i)))
			{
				noDuplicates.add(list.get(i));
			}
		}
		
		return noDuplicates;
	}
	
	/*
	 * Reads a text file line by line from the resources folder and
	 * writes the content into a List<String> object
	 * 
	 *  @param fileName	name of the text file
	 *  @return list List<String> object containing the text file input
	 */
	public static List<String> readListFromFile(String fileName)
	{
		App instance = new App();
		List<String> list = new ArrayList<String>();
		String line;
		ClassLoader classLoader = instance.getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());
		
        try (Scanner scanner = new Scanner(file)) 
        {
    		while (scanner.hasNextLine()) 
    		{
    			line = scanner.nextLine();
    			list.add(line);
    		}

    		scanner.close();
    	} 
        
        catch (IOException e) 
        {
    		e.printStackTrace();
    	}
		
		return list;
	}
}
