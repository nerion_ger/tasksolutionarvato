package com.passmann;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class App {

	public static void main(String[] args) {
		
		App instance = new App();
		
		List<String> list1 = CompareLists.readListFromFile("List1.txt");
		List<String> list2 = CompareLists.readListFromFile("List2.txt");
		List<String> onlyInList1 = new ArrayList<String>();
		List<String> onlyInList2 = new ArrayList<String>();
		List<String> inBothLists = new ArrayList<String>();
		
		JSONObject obj = new JSONObject();
		JSONArray jsonArrayList = new JSONArray();
		
		list1 = CompareLists.removeDuplicates(list1);
		list2 = CompareLists.removeDuplicates(list2);

		onlyInList1 = CompareLists.checkUniqueOccurence(list1, list2);
		onlyInList2 = CompareLists.checkUniqueOccurence(list2, list1);
		inBothLists = CompareLists.checkMatchesInBothLists(list1, list2);
		
		// All three lists will be converted into a JSON and shown on console
		
		for(int i = 0; i < onlyInList1.size(); i++)
		{
			jsonArrayList.add(onlyInList1.get(i));
		}
		
		obj.put("onlyInList1", jsonArrayList);
		jsonArrayList = new JSONArray();
		
		for(int i = 0; i < onlyInList2.size(); i++)
		{
			jsonArrayList.add(onlyInList2.get(i));
		}
		
		obj.put("onlyInList2", jsonArrayList);
		jsonArrayList = new JSONArray();
		
		for(int i = 0; i < inBothLists.size(); i++)
		{
			jsonArrayList.add(inBothLists.get(i));
		}
		
		obj.put("inBothLists", jsonArrayList);
		
		System.out.println(obj);
	}

	

}
