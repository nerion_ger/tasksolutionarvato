package com.passmann;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CompareListsTest 
{
	List<String> list1 = new ArrayList<String>();
	List<String> list2 = new ArrayList<String>();
	List<String> onlyInList1 = new ArrayList<String>();
	List<String> onlyInList2 = new ArrayList<String>();
	List<String> inBothLists = new ArrayList<String>();
	List<String> list1WithoutDuplicates = new ArrayList<String>();
	
    @Before
    public void init()
    {
    	list1.add("Robert Plant");
    	list1.add("Robert Plant");
    	list1.add("Ian Anderson");
    	list1.add("Eddie Van Halen");
    	list1.add("Axl Rose");
    	
    	list2.add("Ronny James Dio");
    	list2.add("Ozzy Osbourne");
    	list2.add("Ian Anderson");
    	list2.add("Steven Tyler");
    	list2.add("Axl Rose");
    	
    	onlyInList1.add("Robert Plant");
    	onlyInList1.add("Eddie Van Halen");
    	
    	onlyInList2.add("Ronny James Dio");
    	onlyInList2.add("Ozzy Osbourne");
    	onlyInList2.add("Steven Tyler");
    	
    	inBothLists.add("Ian Anderson");
    	inBothLists.add("Axl Rose");
    	
    	list1WithoutDuplicates.add("Robert Plant");
    	list1WithoutDuplicates.add("Ian Anderson");
    	list1WithoutDuplicates.add("Eddie Van Halen");
    	list1WithoutDuplicates.add("Axl Rose");
    }
    
    @Test
    public void checkUniqueOccurenceTest()
    {
    	assertEquals(onlyInList1, CompareLists.removeDuplicates(CompareLists.checkUniqueOccurence(list1, list2)));
    	assertEquals(onlyInList2, CompareLists.removeDuplicates(CompareLists.checkUniqueOccurence(list2, list1)));
    }
    
    @Test
    public void checkMatchesInBothListsTest()
    {
    	List<String> list1NoDups = CompareLists.removeDuplicates(list1);
    	List<String> list2NoDups = CompareLists.removeDuplicates(list2);
    	
    	assertEquals(inBothLists, CompareLists.checkMatchesInBothLists(list1NoDups, list2NoDups));
    }
    
    @Test
    public void removeDuplicatesTest()
    {
    	assertEquals(list1WithoutDuplicates, CompareLists.removeDuplicates(list1));
    }
}
