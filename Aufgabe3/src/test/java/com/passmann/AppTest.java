package com.passmann;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;

public class AppTest 
{
	List<Flear> flears = new ArrayList<Flear>();
	
	@Before
	public void init()
	{
		
		
		flears.add(new Flear("1", 94, 6));
		flears.add(new Flear("2", 76, 2));
		flears.add(new Flear("3", 31, 4));
		flears.add(new Flear("4", 32, 3));
		flears.add(new Flear("5", 11, 6));
	}
	
    @Test
    public void getOptimalValueTest()
    {
    	assertEquals(16, App.getOptimalValue(150, this.flears));
    }
}
