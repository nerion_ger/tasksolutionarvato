package com.passmann;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class App
{

	public static void main(String[] args)
	{
		List<Flear> flears = randomFill(10);
		int optimalValue = getOptimalValue(150, flears);
		
		System.out.println("Optimal Value: " + optimalValue);
	}

	public static int getOptimalValue(float pMoney, List<Flear> flears)
	{
		int optimalValue = 0;
		int index = 0;
		float money = pMoney;
		float moneyTemp = 0;
		
		List<List<Flear>> outerArray = new ArrayList<List<Flear>>();
		int[] arrRatingsTotal = new int[flears.size()];
		
		for(int i = 0; i < flears.size(); i++)
		{
			outerArray.add(flears);
		}
		
		//Combine all possibilities with rankings and prices
		for(int i = 0; i < outerArray.size(); i++)
		{
			List<Flear> flearsInsideArray = new ArrayList<Flear>();
			flearsInsideArray = outerArray.get(i);
			index = i;
			moneyTemp = money;

			for(int j = 0; j < flearsInsideArray.size(); j++)
			{				
				if(flearsInsideArray.get(index).getPrice() <= moneyTemp)
				{
					moneyTemp -= flearsInsideArray.get(index).getPrice();
					arrRatingsTotal[index] += flearsInsideArray.get(index).getRating();
				}
				
				if(index != j)
				{
					if(flearsInsideArray.get(j).getPrice() <= moneyTemp)
					{
						moneyTemp -= flearsInsideArray.get(j).getPrice();
						arrRatingsTotal[index] += flearsInsideArray.get(index).getRating();
					}
				}
			}
		}
		
		for(int i = 0; i < arrRatingsTotal.length; i++)
		{
			if(arrRatingsTotal[i] > optimalValue)
			{
				optimalValue = arrRatingsTotal[i];
			}
		}
		
		return optimalValue;
	}
	
	//Fill the Flear list with random values
	private static List<Flear> randomFill(int amount)
    {
    	Random rnd = new Random();
    	int rating = 0;
    	float price = 0;
    	String name = "";
    	
    	List<Flear> flears = new ArrayList<Flear>(amount);
    	
    	for(int i = 0; i < amount; i++)
    	{
    		rating = rnd.nextInt(10) + 1;
    		price = rnd.nextFloat() * 100;
    		name = Integer.toString(i);
    		flears.add(new Flear(name, price, rating));
    	}
    	
    	return flears;
    }
}
