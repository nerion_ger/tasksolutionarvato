package com.passmann;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;

public class JSONAnalyzerTest 
{
	String jsonArray;
	String mostPopularProduct;
	String cheapestProduct;
	String mostExpensiveProduct;
	boolean fragile;
	JSONArray germanProducts;
	JSONArray chineseProducts;
	
	@Before
	public void init()
	{
	    jsonArray = "[{\"ddat\": \"product1\",\"countryOfOrigin\": \"DE\",\"price\": 123.50,\"isFragile\": true,\"timesPurchased\": 150},{\"ddat\": \"product2\",\"countryOfOrigin\": \"DE\",\"price\": 45,\"isFragile\": false,\"timesPurchased\": 1241},{\"ddat\": \"product3\",\"countryOfOrigin\": \"CN\",\"price\": 13,\"isFragile\": false,\"timesPurchased\": 772}]";
		mostPopularProduct = "product2";
		cheapestProduct = "product3";
		mostExpensiveProduct = "product1";
		fragile = true;
		germanProducts = new JSONArray();
		chineseProducts = new JSONArray();
		
		germanProducts.add("product1");
		germanProducts.add("product2");
		chineseProducts.add("product3");
	}
	
	@Test
	public void getMostExpensiveProductTest()
	{
		assertEquals(mostExpensiveProduct, JSONAnalyzer.getMostExpensiveProduct(jsonArray));
	}
	
	@Test
	public void getCheapestProductTest()
	{
		assertEquals(cheapestProduct, JSONAnalyzer.getCheapestProduct(jsonArray));
	}
	
	@Test
	public void getMostPopularProductTest()
	{
		assertEquals(mostPopularProduct, JSONAnalyzer.getMostPopularProduct(jsonArray));
	}
	
	@Test
	public void getGermanProductsTest()
	{
		assertEquals(germanProducts, JSONAnalyzer.getGermanProducts(jsonArray, "DE"));
	}
	
	@Test
	public void getChineseProductsTest()
	{
		assertEquals(chineseProducts, JSONAnalyzer.getChineseProducts(jsonArray, "CN"));
	}
	
	@Test
	public void hasFragileProductsTest()
	{
		assertTrue(JSONAnalyzer.hasFragileProducts(jsonArray));
	}
	
}
