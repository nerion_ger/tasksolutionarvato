package com.passmann;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONAnalyzer {
	
	/*
	 * Reads a text json file and
	 * writes the content into a string variable
	 * 
	 *  @param fileName	name of the text file
	 *  @return strFile returns a string variable containing json information
	 */
	public static String loadJSONFile(String fileName)
	{
		JSONAnalyzer instance = new JSONAnalyzer();
		ClassLoader loader = instance.getClass().getClassLoader();
		File file = new File(loader.getResource(fileName).getFile());
		
		String strFile = "";
		
		try(Scanner scanner = new Scanner(file))
		{
			while(scanner.hasNextLine())
			{
				strFile += scanner.nextLine();
			}
			
			scanner.close();
		}
		
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		
		return strFile;
	}
	
	//Converts the passed string into a JSONArray object
	private static JSONArray convertStringToJSONArray(String jsonArray)
	{
		JSONParser parser = new JSONParser();
		JSONArray array = new JSONArray();
		
		try 
		{
			array = (JSONArray)parser.parse(jsonArray);
		} 
		
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		
		return array;
	}
	
	//Searches for the highest value from a specific property by iterating through the json objects inside the json array
	private static String getMax(String jsonArray, String propertyName)
	{
		JSONArray array = JSONAnalyzer.convertStringToJSONArray(jsonArray);
		JSONObject obj = new JSONObject();
		String productName = "";
		double maxValue = 0.0;
		double currentValue = 0.0;
		
		for(int i = 0; i < array.size(); i++)
		{
			obj = (JSONObject)array.get(i);
			currentValue = Double.parseDouble(obj.get(propertyName).toString());
			
			if(currentValue > maxValue)
			{
				productName = obj.get("ddat").toString();
				maxValue = currentValue;
			}
		}
		
		return productName;
	}
	
	//Searches for the lowest value from a specific property by iterating through the json objects inside the json array
	private static String getMin(String jsonArray, String propertyName)
	{
		JSONArray array = JSONAnalyzer.convertStringToJSONArray(jsonArray);
		JSONObject obj;
		String productName = "";
		double lowestValue = Double.MAX_VALUE;
		double currentValue = 0.0;
		
		for(int i = 0; i < array.size(); i++)
		{
			obj = (JSONObject)array.get(i);
			currentValue = Double.parseDouble(obj.get(propertyName).toString());
			
			if(currentValue < lowestValue)
			{
				productName = obj.get("ddat").toString();
				lowestValue = currentValue;
			}
		}
		
		return productName;
	}
	
	//Collects all product names which have the same country code (parameter)
	private static JSONArray getArrayList(String jsonArray, String propertyName, String countryCode)
	{
		JSONArray list = new JSONArray();
		JSONArray array = JSONAnalyzer.convertStringToJSONArray(jsonArray);
		JSONObject obj = new JSONObject();
		String countryOfOrigin = "";
		
		for(int i = 0; i < array.size(); i++)
		{
			obj = (JSONObject)array.get(i);
			countryOfOrigin = obj.get(propertyName).toString();
			
			if(countryOfOrigin.equals(countryCode))
			{
				list.add(obj.get("ddat").toString());
			}
		}
		
		return list;
	}
	
	/*
	 * Returns a product with the most expensive price
	 * 
	 * @param jsonArray A string containing json information
	 * @return Returns a string/product with the most expensive price
	 */
	public static String getMostExpensiveProduct(String jsonArray)
	{
		return JSONAnalyzer.getMax(jsonArray, "price");
	}
	
	/*
	 * Returns a product with the cheapest price
	 * 
	 * @param jsonArray A string containing json information
	 * @return Returns a string/product with the cheapest price
	 */
	public static String getCheapestProduct(String jsonArray)
	{
		return JSONAnalyzer.getMin(jsonArray, "price");
	}
	
	/*
	 * Returns the most popular product
	 * 
	 * @param jsonArray A string containing json information
	 * @return Returns a string/product which is the most popular
	 */
	public static String getMostPopularProduct(String jsonArray)
	{
		return JSONAnalyzer.getMax(jsonArray, "timesPurchased");
	}
	
	/*
	 * Collects all german products
	 * 
	 * @param jsonArray A string containing json information
	 * @return Returns a JSONArray object with all german products
	 */
	public static JSONArray getGermanProducts(String jsonArray, String countryCode)
	{
		return JSONAnalyzer.getArrayList(jsonArray, "countryOfOrigin", countryCode);
	}
	
	/*
	 * Collects all chinese products
	 * 
	 * @param jsonArray A string containing json information
	 * @return Returns a JSONArray object with all chinese products
	 */
	public static JSONArray getChineseProducts(String jsonArray, String countryCode)
	{
		return JSONAnalyzer.getArrayList(jsonArray, "countryOfOrigin", countryCode);
	}
	
	/*
	 * Checks if there is any fragile product
	 * 
	 * @param json Array A string containing json information
	 * @return fragile Returns a boolean which says if there are any fragile products or not
	 */
	public static boolean hasFragileProducts(String jsonArray)
	{
		boolean fragile = false;
		
		JSONArray array = JSONAnalyzer.convertStringToJSONArray(jsonArray);
		JSONObject obj = new JSONObject();
		
		for(int i = 0; i < array.size(); i++)
		{
			obj = (JSONObject)array.get(i);
			fragile = Boolean.parseBoolean(obj.get("isFragile").toString());
			
			if(fragile)
			{
				return true;
			}
		}
		
		return fragile;
	}
}
