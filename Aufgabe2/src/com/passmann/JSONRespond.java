package com.passmann;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.json.simple.*;

@Path("/analyze")
public class JSONRespond {

	@GET
	@Produces("application/json")
	public Response respond()
	{
		JSONObject obj = new JSONObject();
		String file = JSONAnalyzer.loadJSONFile("sampleProductsData.json");
		
		String mostExpensiveProduct = JSONAnalyzer.getMostExpensiveProduct(file);
		String cheapestProduct = JSONAnalyzer.getCheapestProduct(file);
		String mostPopularProduct = JSONAnalyzer.getMostPopularProduct(file);
		JSONArray germanProducts = JSONAnalyzer.getGermanProducts(file, "DE");
		JSONArray chineseProducts = JSONAnalyzer.getChineseProducts(file, "CN");
		boolean hasFragileProducts = JSONAnalyzer.hasFragileProducts(file);
		
		//create JSONObject with all filtered information
		obj.put("mostExpensiveProduct", mostExpensiveProduct);
		obj.put("cheapestProduct", cheapestProduct);
		obj.put("mostPopularProduct", mostPopularProduct);
		obj.put("germanProducts", germanProducts);
		obj.put("chineseProducts", chineseProducts);
		obj.put("hasFragileProducts", hasFragileProducts);
		
		return Response.status(200).entity(obj.toString()).build();
	}
}
